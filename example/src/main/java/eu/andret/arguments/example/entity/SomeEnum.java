/*
 * Copyright (c) 2018 Andret Tools System. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.arguments.example.entity;

public enum SomeEnum {
	ONE,
	TWO,
	THREE
}
