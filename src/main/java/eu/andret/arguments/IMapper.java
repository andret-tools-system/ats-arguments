/*
 * Copyright (c) 2018 Andret Tools System. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.arguments;

/**
 * An universal mapper interface.
 *
 * @author Andret
 * @since Apr 19, 2020
 */
public interface IMapper {
}
