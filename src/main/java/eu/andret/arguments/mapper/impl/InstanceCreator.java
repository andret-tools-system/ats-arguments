/*
 * Copyright (c) 2018 Andret Tools System. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.arguments.mapper.impl;

import eu.andret.arguments.AnnotatedCommandExecutor;
import eu.andret.arguments.mapper.IInstanceCreator;
import lombok.SneakyThrows;
import lombok.Value;
import lombok.experimental.NonFinal;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Constructor;
import java.util.Optional;

/**
 * The implementation of {@link InstanceCreator}.
 *
 * @author Andret
 * @since Sep 03, 2021
 */
@Value
@NonFinal
public class InstanceCreator implements IInstanceCreator {
	@Override
	@NotNull
	@SneakyThrows
	public <E extends JavaPlugin, A extends AnnotatedCommandExecutor<E>> A createInstance(
			@NotNull final CommandSender sender,
			@NotNull final JavaPlugin plugin,
			@NotNull final Class<A> executor,
			@NotNull final Object... parameters) {
		final Constructor<A> constructor = findConstructor(executor, plugin)
				.orElseThrow(() -> new IllegalStateException("AnnotatedCommandExecutor subclass needs a constructor with at least 2 parameters: CommandSender and JavaPlugin as first two of them"));
		final Object[] arguments = new Object[parameters.length + 2];
		arguments[0] = sender;
		arguments[1] = plugin;
		System.arraycopy(parameters, 0, arguments, 2, parameters.length);
		return constructor.newInstance(arguments);
	}

	@NotNull
	@SuppressWarnings({"unchecked", "java:S1612"})
	private <E extends JavaPlugin, A extends AnnotatedCommandExecutor<E>> Optional<Constructor<A>> findConstructor(
			@NotNull final Class<A> executor,
			@NotNull final JavaPlugin plugin) {
		final Constructor<A>[] constructors = (Constructor<A>[]) executor.getDeclaredConstructors();
		if (constructors.length != 1) {
			throw new UnsupportedOperationException("The class " + executor.getName()
					+ " has to have exactly one declared constructor");
		}
		return Optional.of(constructors[0])
				.filter(constructor -> constructor.getParameterCount() >= 2)
				.filter(constructor -> constructor.getParameterTypes()[0].isAssignableFrom(CommandSender.class))
				.filter(constructor -> constructor.getParameterTypes()[1].isAssignableFrom(plugin.getClass()));
	}
}
