/*
 * Copyright (c) 2018 Andret Tools System. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.arguments.api.annotation;

import eu.andret.arguments.api.entity.FallbackPriority;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Using this annotation allows to decide which method should execute when an {@link Argument} annotated command param
 * matches its fallback condition.
 *
 * @author Andret
 * @since Jun 10, 2020
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface TypeFallback {
	/**
	 * The array of {@link Class} values which will fall back into the annotated method if they meet their fallback
	 * conditions.
	 *
	 * @return The list of values.
	 */
	Class<?>[] value();

	/**
	 * The {@link FallbackPriority} to determine the call order. Methods with equal priority will be called randomly.
	 *
	 * @return The method's priority.
	 */
	FallbackPriority priority() default FallbackPriority.NORMAL;
}
