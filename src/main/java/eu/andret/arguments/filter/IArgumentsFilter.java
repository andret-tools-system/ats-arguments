/*
 * Copyright (c) 2018 Andret Tools System. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.arguments.filter;

import eu.andret.arguments.IMapper;

import java.lang.reflect.Method;

/**
 * An interface for mapping arguments.
 *
 * @author Andret
 * @since Apr 17, 2020
 */
@FunctionalInterface
public interface IArgumentsFilter extends IMapper {
	/**
	 * @param method The {@link Method} that will be analyzed.
	 * @param command The array of {@link String} with arguments passed with command.
	 *
	 * @return {@code true} if {@code method} matches command arguments and mappers input arguments, {@code false}
	 * 		otherwise.
	 */
	boolean filterArguments(Method method, String[] command);
}
