/*
 * Copyright (c) 2018 Andret Tools System. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.arguments.entity;

// nothing special, just some random values for testing purpose
public enum SomeEnum {
	TEST,
	VALUE,
	RESULT
}
