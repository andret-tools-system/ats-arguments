/*
 * Copyright (c) 2018 Andret Tools System. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.arguments.provider;

public enum TestEnum {
	TEST_ONE,
	TEST_TWO,
	TEST_THREE
}
